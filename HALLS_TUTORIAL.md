# README #

gem5_halls is a modified gem5 simulator to simulate the behavior of a reduced retention time STT-RAM cache with a focus on the L2 cache. The modifications are based on gem5 stable version 2015_09_13. The official gem5 repo can be checked out at: https://gem5.googlesource.com/public/gem5

How do I get set up?
--------------------

### gem5 basic ###

Please refer to gem5 get started page http://www.gem5.org/Documentation#Getting_Started for how to install gem5.

### Configuration ###
gem5_halls supports two L2 STT-RAM cache scenarios:

1. Expired-Invalid: Check the time period of the block's insertion tick and current tick. If the period is greater than the retention time, the block is evicted. The block would be invalidated if it is clean, or written back to lower level memory if dirty. HALLS allows multi-retention-time cluster simulation (up to 4 retention times). 

		Use options: l2_ret0, l2_ret1, l2_ret2, l2_ret3

2. Dynamic Refresh Scheme: Specify the STT-RAM retention/refresh time, whenever the block's lifetime reaches the refresh time, the cache's refresh counter increments by one to indicate the number of refreshes. HALLS provides uniform retention time simualtion for DRS.
		
		Use options: l2_rfrsh
		variables in stats.txt:	l2.numSTTRefresh, l2.numSTTRefresh

The unit of refresh time and retention time is in ticks, where 1000 ticks represent 1ns.
Default value of the options above are specified in **configs/common/Caches.py** 

### Tutorial ###

For specifying the retention time, nanoseconds, microseconds, milliseconds, and seconds are represented by k, M, G, and T, respectively.

1. Expired-Invalid

	The command below sets four retention times 100us, 1ms, 10ms, 100ms for the four 32KB cache banks in a 128KB 4-way L2 cache. 100us, 1ms, 10ms, 100ms would be used in way0, way1, way2, and way3, respectively.
	
		./build/ARM/gem5.opt ./configs/example/se.py --caches --l1i_size=32kB --l1i_assoc=4 --l1d_size=32kB --l1d_assoc=4 --l2cache --l2_assoc=4 --l2_size=128kB --l2_ret0=100M --l2_ret1=1G --l2_ret2=10G --l2_ret3=100G --cacheline_size=64 --cpu-clock=2GHz --mem-size=8192MB --cpu-type=timing -n 4 -c ./tests/test-progs/hello/bin/arm/linux/hello

	Change the retention time (**l2_ret0**, **l2_ret1**, **l2_ret2**, **l2_ret3**) and compare cache misses in m5out/stats.txt


2. Dynamic Refresh Scheme

	The command below command sets the L2 refresh time to 10ms

		./build/ARM/gem5.opt ./configs/example/se.py --caches --l1i_size=32kB --l1i_assoc=4 --l1d_size=32kB --l1d_assoc=4 --l2cache --l2_assoc=4 --l2_size=128kB --l2_rfrsh=10G --cacheline_size=64 --cpu-clock=2GHz --mem-size=8192MB --cpu-type=timing -n 4 -c ./tests/test-progs/hello/bin/arm/linux/hello

	Change the refresh time (**l2_rfrsh**) and compare l2.numSTTRefresh, l2.numSTTRefresh_bank# in m5out/stats.txt. For L2 cache, you might want to set as low as 10us to see the change.

Who do I talk to? 
-----------------
gem5_halls was developed by Kyle Kuan <ckkuan@email.arizona.edu> and Tosiron Adegbija <tosiron@email.arizona.edu>. 
